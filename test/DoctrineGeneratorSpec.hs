{-# LANGUAGE OverloadedStrings #-}

module DoctrineGeneratorSpec (spec) where

import Data.Text (Text)
import DoctrineGenerator
import Test.Hspec
import Types
import PhpGenerator

default (Text)

spec :: Spec
spec = do
    relationshipPropertyCases

relationshipPropertyCases :: Spec
relationshipPropertyCases = do
    describe "outgoing relationship" $ do
        -- foreign key picture.user_id → user.id
        let relationship = Relationship {relationshipDirection = Outgoing, relationshipSourceColumn = ColumnRef {refTable = "Picture", refColumn = "user_id"}, relationshipTargetColumn = ColumnRef {refTable = "User", refColumn = "id"}}
        let output =
              Property
                { propertyVisibility = Protected
                , propertyName = "user"
                , propertyComment = Just "@ORM\\ManyToOne(targetEntity=\"User\", inversedBy=\"pictures\")\n@ORM\\JoinColumn(name=\"user_id\", referencedColumnName=\"id\")"
                }

        it "is converted to class property declaration" $
            relationshipProperty relationship `shouldBe` output

    describe "incoming relationship" $ do
        -- referenced column user.id ← picture.user_id
        let relationship = Relationship {relationshipDirection = Incoming, relationshipSourceColumn = ColumnRef {refTable = "User", refColumn = "id"}, relationshipTargetColumn = ColumnRef {refTable = "Picture", refColumn = "user_id"}}
        let output =
              Property
                { propertyVisibility = Protected
                , propertyName = "pictures"
                , propertyComment = Just "@ORM\\OneToMany(targetEntity=\"Picture\", mappedBy=\"user\")"
                }

        it "is converted to class property declaration" $
            relationshipProperty relationship `shouldBe` output
