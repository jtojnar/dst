{-# LANGUAGE OverloadedStrings #-}

module ParserSpec (spec) where

import qualified Data.Map as M
import Data.Text (Text)
import Parser
import Test.Hspec
import qualified Text.XML as Xml
import Text.XML.Cursor (Cursor, descendant, element, fromDocument)
import Data.Default.Class (def)
import Types
import Paths_dst (getDataFileName)

default (Text)

spec :: Spec
spec = do
    simpleRelationshipsCases

projectFixture :: IO Cursor
projectFixture = do
    path <- getDataFileName "test/fixtures/project.xml"
    doc <- Xml.readFile def path
    return (fromDocument doc)

simpleRelationshipsCases :: Spec
simpleRelationshipsCases = do
    let fks = M.fromList [("QEv3ExaD.AACARGf", (ColumnRef {refTable = "Picture", refColumn = "user_id"}, ColumnRef {refTable = "User", refColumn = "id"}))]
    fixture <- runIO projectFixture
    let input = descendant fixture >>= element "DBTable"

    describe "outgoing relationship" $ do
        let relationship = Relationship {relationshipDirection = Outgoing, relationshipSourceColumn = ColumnRef {refTable = "Picture", refColumn = "user_id"}, relationshipTargetColumn = ColumnRef {refTable = "User", refColumn = "id"}}

        it "is parsed correctly" $
            (input >>= \cur -> simpleRelationships fks cur Outgoing) `shouldBe` [relationship]

    describe "incoming relationship" $ do
        let relationship = Relationship {relationshipDirection = Incoming, relationshipSourceColumn = ColumnRef {refTable = "User", refColumn = "id"}, relationshipTargetColumn = ColumnRef {refTable = "Picture", refColumn = "user_id"}}

        it "is parsed correctly" $
            (input >>= \cur -> simpleRelationships fks cur Incoming) `shouldBe` [relationship]
