{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Applicative ((<**>))
import Data.Default.Class (def)
import Data.Semigroup ((<>))
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Manipulate as T
import DoctrineGenerator
import qualified Options.Applicative as Opt
import qualified Options.Applicative.Text as Opt
import Parser
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((<.>), (</>))
import System.IO (hPutStrLn, stderr)
import qualified Text.XML as Xml
import Text.XML.Cursor as Xml
import Types

default (Text)

data DstOptions = DstOptions
    { input :: String
    , namespace :: Text
    , output :: String
    }

dstOptions :: Opt.Parser DstOptions
dstOptions = DstOptions
      <$> Opt.strOption
          ( Opt.long "input"
         <> Opt.metavar "FILE"
         <> Opt.help "XML file containing a database schema" )
      <*> Opt.textOption
          ( Opt.long "namespace"
         <> Opt.metavar "NS"
         <> Opt.help "PHP namespace for generated entities" )
      <*> Opt.strOption
          ( Opt.long "output"
         <> Opt.metavar "DIR"
         <> Opt.help "Directory where entity files will be placed" )

opts :: Opt.ParserInfo DstOptions
opts = Opt.info (dstOptions <**> Opt.helper)
    ( Opt.fullDesc
    <> Opt.progDesc "Database schema translator"
    <> Opt.header "dst" )

main :: IO ()
main = do
    options@(DstOptions {input, output}) <- Opt.execParser opts
    doc <- Xml.readFile def input

    let cursor = Xml.fromDocument doc
    let colrefs = columnRefs cursor
    let fks = foreignKeys colrefs cursor
    let dbTables = tables fks cursor

    createDirectoryIfMissing True output
    mapM_ (writeTableClass options) dbTables

writeTableClass :: DstOptions -> Table -> IO ()
writeTableClass DstOptions {namespace, output} table = do
    let path = output </> T.unpack (T.toPascal (tableName table)) <.> "php"
    hPutStrLn stderr ("Generating " ++ path ++ "…")
    T.writeFile path (printTableClass namespace table)
