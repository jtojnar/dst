{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}

module Types where

import Data.String (IsString)
import Data.Text (Text)

newtype Id = Id
    { fromId :: Text
    } deriving (Eq, Ord, IsString)

instance Show Id where
    show Id {fromId} = show fromId

data RelationshipDirection =
    Outgoing -- Foreign key
    | Incoming -- Column pointed to by foreign key
    deriving (Show, Eq)

isOutgoing :: RelationshipDirection -> Bool
isOutgoing Outgoing = True
isOutgoing Incoming = False

data ColumnRef = ColumnRef
    { refTable :: Text
    , refColumn :: Text
    } deriving (Show, Eq)

data Relationship = Relationship
    { relationshipDirection :: RelationshipDirection
    , relationshipSourceColumn :: ColumnRef
    , relationshipTargetColumn :: ColumnRef
    } deriving (Show, Eq)

newtype Project = Project
    { projectTables :: [Table]
    } deriving (Show, Eq)

data Table = Table
    { tableId :: Id
    , tableName :: Text
    , tableDescription :: Text
    , tableColumns :: [Column]
    , tableRelationships :: [Relationship]
    } deriving (Show, Eq)

data Column = Column
    { columnId :: Id
    , columnIdGenerator :: Text
    , columnName :: Text
    , columnDescription :: Text
    , columnNullable :: Bool
    , columnPrimaryKey :: Bool
    , columnType :: Text
    , columnLength :: Text
    , columnUnique :: Bool
    } deriving (Show, Eq)
