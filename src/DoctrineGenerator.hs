{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module DoctrineGenerator where

import Data.List (partition)
import Data.Maybe (fromMaybe)
import Data.Semigroup ((<>))
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Manipulate as T
import PhpGenerator
import Text.Countable (pluralize)
import Types

default (Text)

printTableClass :: Text -> Table -> Text
printTableClass namespace Table {tableName, tableColumns, tableRelationships, tableDescription} =
    let
        name = T.toPascal tableName
        sqlName = T.toSnake tableName
        (outgoingRelationships, incomingRelationships) = partition (isOutgoing . relationshipDirection) tableRelationships
        isOutColumn Column {columnName} = columnName `elem` map (refColumn . relationshipSourceColumn) outgoingRelationships

        -- outgoing columns (foreign keys) are already in relationshipProperties
        nonOutgoingColumns = filter (not . isOutColumn) tableColumns
        relationshipProperties = map relationshipProperty tableRelationships
        imports = ((if not (null incomingRelationships) then ["Doctrine\\Common\\Collections\\ArrayCollection", "Doctrine\\Common\\Collections\\Collection" ] else []) ++ [ "Doctrine\\ORM\\Mapping as ORM", "Kdyby\\Doctrine\\Entities\\BaseEntity"])
        description = if T.null tableDescription then "" else " – " <> tableDescription
    in printFile File
        { fileNamespace = Just (ClassPath namespace)
        , fileStrict = True
        , fileImports = imports
        , fileClass = Class
            { classExtends = Just (ClassPath "BaseEntity")
            , className = name
            , classComment = Just (name <> description <> ".\n\n@ORM\\Entity\n@ORM\\Table(name=\"" <> sqlName <> "\")")
            , classTraits = []
            , classProperties = map mkColumnProperty nonOutgoingColumns ++ relationshipProperties
            , classMethods = mkMethods nonOutgoingColumns incomingRelationships
            }
        }

mkMethods :: [Column] -> [Relationship] -> [Method]
mkMethods nonOutgoingColumns incomingRelationships =
    let
        relationshipCollectionsConstruction = T.intercalate "\n" (["parent::__construct();"] <> map (\rel -> "$this->" <> T.toCamel (incomingRelationshipPropertyName rel) <> " = new ArrayCollection();") incomingRelationships)

        constructor = Method
            { methodVisibility = Public
            , methodName = "__construct"
            , methodComment = Nothing
            , methodReturnTypeHint = Nothing
            , methodArguments = []
            , methodBody = relationshipCollectionsConstruction
            }

        nonOutgoingMethods = concatMap (\Column {columnName, columnType} -> [mkGetter columnName columnType, mkSetter columnName columnType]) nonOutgoingColumns

        incomingMethods = map (\rel -> mkGetter (incomingRelationshipPropertyName rel) "Collection") incomingRelationships

    in [constructor] ++ nonOutgoingMethods ++ incomingMethods

mkGetter :: Text -> Text -> Method
mkGetter propertyName propertyType =
    let
        pascalName = T.toPascal propertyName
        camelName = T.toCamel propertyName
        phpType = toPhpType propertyType
    in Method
        { methodVisibility = Public
        , methodName = "get" <> pascalName
        , methodComment = Nothing
        , methodReturnTypeHint = Just phpType
        , methodArguments = []
        , methodBody = "return $this->" <> camelName <> ";"
        }

mkSetter :: Text -> Text -> Method
mkSetter propertyName propertyType =
    let
        pascalName = T.toPascal propertyName
        camelName = T.toCamel propertyName
        phpType = toPhpType propertyType
    in Method
        { methodVisibility = Public
        , methodName = "set" <> pascalName
        , methodComment = Nothing
        , methodReturnTypeHint = Just "void"
        , methodArguments = [
            Argument
                { argumentName = camelName
                , argumentTypeHint = Just phpType
                , argumentDefaultValue = Nothing
                }
            ]
        , methodBody = "$this->" <> camelName <> " = $" <> camelName <> ";"
        }

relationshipProperty :: Relationship -> Property
relationshipProperty relationship@Relationship {relationshipDirection, relationshipSourceColumn, relationshipTargetColumn} =
    let
        pname = case relationshipDirection of
            Incoming -> incomingRelationshipPropertyName relationship
            Outgoing -> outgoingRelationshipPropertyName relationshipSourceColumn
        description = ""
        annots = case relationshipDirection of
            Incoming ->
                [
                    "@ORM\\OneToMany(targetEntity=\"" <> refTable relationshipTargetColumn <> "\", mappedBy=\"" <> outgoingRelationshipPropertyName relationshipTargetColumn <> "\")"
                ]
            Outgoing ->
                [
                    "@ORM\\ManyToOne(targetEntity=\"" <> refTable relationshipTargetColumn <> "\", inversedBy=\"" <> T.toCamel (pluralize (refTable relationshipSourceColumn)) <> "\")",
                    "@ORM\\JoinColumn(name=\"" <> refColumn relationshipSourceColumn <> "\", referencedColumnName=\"" <> refColumn relationshipTargetColumn <> "\")"
                ]
    in mkProperty pname description annots

incomingRelationshipPropertyName :: Relationship -> Text
incomingRelationshipPropertyName relationship = pluralize (refTable (relationshipTargetColumn relationship))

outgoingRelationshipPropertyName :: ColumnRef -> Text
outgoingRelationshipPropertyName ref =
    let
        columnName = refColumn ref
    in fromMaybe columnName (T.stripSuffix "_id" columnName)

mkColumnProperty :: Column -> Property
mkColumnProperty column@Column {columnName, columnDescription} =
    mkProperty columnName columnDescription (columnAnnotations column)

mkProperty :: Text -> Text -> [Text] -> Property
mkProperty name description annotations =
    let
        camelName = T.toCamel name
        commentLines = (if T.null description then [] else [description, ""]) ++ annotations
    in Property
        { propertyVisibility = Protected
        , propertyName = camelName
        , propertyComment = Just (T.intercalate "\n" commentLines)
        }

columnAnnotations :: Column -> [Text]
columnAnnotations column =
    let
        idAnnotations =
            if columnName column == "id" then [
                "@ORM\\Id",
                "@ORM\\GeneratedValue"
            ] else []

        extraColumnProps =
            ["length=" <> columnLength column | columnType column `elem` ["varchar", "decimal", "numeric"] && columnLength column /= "0"]

        columnProps = ["type=\"" <> toDoctrineType (columnType column) <> "\""] <> extraColumnProps

        typeAnnotation =
            "@ORM\\Column(" <> T.intercalate ", " columnProps <>")"
    in
        idAnnotations ++ [typeAnnotation]

-- | Convert type from ERD to one that can be used in PHP.
-- https://www.postgresql.org/docs/current/datatype.html
toPhpType :: Text -> Text
toPhpType "smallint" = "int"
toPhpType "integer" = "int"
toPhpType "bigint" = "int"
toPhpType "decimal" = "string"
toPhpType "numeric" = "string"
toPhpType "int2" = "int"
toPhpType "int4" = "int"
toPhpType "int8" = "int"
toPhpType "varchar" = "string"
toPhpType "longtext" = "string"
toPhpType "text" = "string"
toPhpType "boolean" = "bool"
toPhpType "datetime" = "\\DateTimeImmutable"
toPhpType "timestamp" = "\\DateTimeImmutable"
toPhpType "time" = "\\DateTimeImmutable"
toPhpType ty = ty

-- | Convert type from ERD to one that can be used in Doctrine
-- column annotations.
-- https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/basic-mapping.html#doctrine-mapping-types
-- https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html
toDoctrineType :: Text -> Text
toDoctrineType "int" = "integer"
toDoctrineType "integer" = "integer"
toDoctrineType "int4" = "int"
toDoctrineType "smallint" = "smallint"
toDoctrineType "int2" = "smallint"
toDoctrineType "bigint" = "bigint"
toDoctrineType "int8" = "bigint"
toDoctrineType "varchar" = "string"
toDoctrineType "text" = "text"
toDoctrineType "longtext" = "text"
toDoctrineType "boolean" = "boolean"
toDoctrineType "bool" = "boolean"
toDoctrineType "decimal" = "decimal"
toDoctrineType "date" = "date_immutable"
toDoctrineType "time" = "time_immutable"
toDoctrineType "datetime" = "datetime_immutable"
toDoctrineType "timestamp" = "datetime_immutable"
toDoctrineType "datetimetz" = "datetimetz_immutable"
toDoctrineType ty = ty
