{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE OverloadedStrings #-}

module PhpGenerator where

import Data.Semigroup ((<>))
import qualified Data.Foldable as Foldable
import Data.String (IsString)
import Data.Text (Text)
import qualified Data.Text as T

default (Text)

newtype ClassPath = ClassPath
    { fromClassPath :: Text
    } deriving (Eq, Ord, IsString)

instance Show ClassPath where
    show ClassPath {fromClassPath} = show fromClassPath

data File = File
    { fileNamespace :: Maybe ClassPath
    , fileStrict :: Bool
    , fileImports :: [ClassPath]
    , fileClass :: Class
    } deriving (Eq, Show)

data Class = Class
    { classExtends :: Maybe ClassPath
    , className :: Text
    , classComment :: Maybe Text
    , classTraits :: [Text]
    , classProperties :: [Property]
    , classMethods :: [Method]
    } deriving (Eq, Show)

data Method = Method
    { methodVisibility :: Visibility
    , methodName :: Text
    , methodComment :: Maybe Text
    , methodReturnTypeHint :: Maybe Text
    , methodArguments :: [Argument]
    , methodBody :: Text
    } deriving (Eq, Show)

data Argument = Argument
    { argumentName :: Text
    , argumentTypeHint :: Maybe Text
    , argumentDefaultValue :: Maybe Text
    } deriving (Eq, Show)

data Property = Property
    { propertyVisibility :: Visibility
    , propertyName :: Text
    , propertyComment :: Maybe Text
    } deriving (Eq, Show)

data Visibility =
    Public
    | Private
    | Protected
    deriving (Eq, Show)

indent :: Text -> Text
indent =
    let
        indentLine line = if T.null line then line else "\t" <> line
    in
        T.unlines . map indentLine . T.lines

printFile :: File -> Text
printFile File {fileNamespace, fileStrict, fileImports, fileClass} =
    T.intercalate "\n\n" (
        ["<?php"]
        ++ ["declare(strict_types=1);" | fileStrict]
        ++ Foldable.toList ["namespace " <> fromClassPath ns <> ";" | ns <- fileNamespace]
        ++ [printUses (map fromClassPath fileImports) | not (null fileImports)]
        ++ [printClass fileClass]
    ) <> "\n"

printClass :: Class -> Text
printClass Class {classExtends, className, classComment, classTraits, classProperties, classMethods} =
    let
        body = T.intercalate "\n\n" ([printUses classTraits | not (null classTraits)] ++ map printProperty classProperties ++ map printMethod classMethods)
        extends = maybe "" (\ClassPath {fromClassPath} -> " extends " <> fromClassPath) classExtends
        comment = maybe "" printDocumentationComment classComment
    in
        comment <>
        "class " <> className <> extends <> "\n{\n" <>
        indent body
        <> "}"

printUses :: [Text] -> Text
printUses = T.intercalate "\n" . map (\t -> "use " <> t <> ";")

printProperty :: Property -> Text
printProperty Property {propertyVisibility, propertyName, propertyComment} =
    let
        comment = maybe "" printDocumentationComment propertyComment
    in
        comment <>
        printVisibility propertyVisibility <> " $" <> propertyName <> ";"

printMethod :: Method -> Text
printMethod Method {methodVisibility, methodName, methodComment, methodReturnTypeHint, methodArguments, methodBody} =
    let
        arguments = T.intercalate ", " (map printArgument methodArguments)
        returnValue = maybe "" (": " <>) methodReturnTypeHint
        comment = maybe "" printDocumentationComment methodComment
    in
        comment <>
        printVisibility methodVisibility <> " function " <> methodName <> "(" <> arguments  <> ")" <> returnValue <> "\n{\n" <>
        indent methodBody
        <> "}"

printArgument :: Argument -> Text
printArgument Argument {argumentName, argumentTypeHint, argumentDefaultValue} =
    let
        defaultValue = maybe "" ("=" <>) argumentDefaultValue
        typeHint = maybe "" (<> " ") argumentTypeHint
    in
        typeHint <> "$" <> argumentName <> defaultValue

printDocumentationComment :: Text -> Text
printDocumentationComment content =
    let
        prefixLines = T.unlines . map (\c -> (if T.null c then " *" else " * ") <> c) . T.lines
    in
        "/**\n" <>
        prefixLines content
        <> " */\n"

printVisibility :: Visibility -> Text
printVisibility Public = "public"
printVisibility Private = "private"
printVisibility Protected = "protected"
