{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Parser where

import Data.Foldable (toList)
import qualified Data.Map as M
import qualified Data.Text as T
import Data.Tuple (swap)
import Text.XML (Name)
import Text.XML.Cursor (Cursor, attribute, child, element)
import Types

default (T.Text)

-- PARSER
--
-- The simple XML file exported from Visual Paradigm has the following structure:
--
-- @
-- Project
--     Models
--         DataType @Id @Name
--         DBTable
--             @Id
--             @Name
--             ModelChildren
--                 DBColumn
--                     @Id
--                     @IdGenerator
--                     @Name
--                     @Documentation_plain
--                     @Nullable
--                     @PrimaryKey
--                     @Type
--                     @Length
--                     @Unique
--                     ForeignKeyConstraints
--                         DBForeignKeyConstraint
--                             @ForeignKey
--                             @RefColumn
--             FromSimpleRelationships
--                 DBForeignKey
--                     @Idref
--             ToSimpleRelationships
--                 DBForeignKey
--                     @Idref
-- @

boolAttribute :: Name -> Cursor -> [Bool]
boolAttribute name cursor = (== "true") <$> attribute name cursor

columnRefs :: Cursor -> M.Map Id ColumnRef
columnRefs cursor = M.fromList $ element "Project" cursor
    >>= child
    >>= element "Models"
    >>= child
    >>= element "DBTable"
    >>= (\tc ->
        let
            tableName = attribute "Name" tc
        in
            child tc
                >>= element "ModelChildren"
                >>= child
                >>= element "DBColumn"
                >>= (\cc ->
                    let
                        columnId = Id <$> attribute "Id" cc
                        columnName = attribute "Name" cc
                    in
                        (\columnId' tableName' columnName' -> (columnId', ColumnRef tableName' columnName'))
                            <$> columnId
                            <*> tableName
                            <*> columnName))

foreignKeys :: M.Map Id ColumnRef -> Cursor -> M.Map Id (ColumnRef, ColumnRef)
foreignKeys colrefs cursor = M.fromList $ element "Project" cursor
    >>= child
    >>= element "Models"
    >>= child
    >>= element "DBTable"
    >>= (\tc ->
        let
            sourceTableName = attribute "Name" tc
        in
            child tc
                >>= element "ModelChildren"
                >>= child
                >>= element "DBColumn"
                >>= (\cc ->
                    let
                        sourceColumnName = attribute "Name" cc
                    in
                        child cc
                            >>= element "ForeignKeyConstraints"
                            >>= child
                            >>= element "DBForeignKeyConstraint"
                            >>= (\fkc ->
                                let
                                    fkId = Id <$> attribute "ForeignKey" fkc
                                    targetColRef = attribute "RefColumn" fkc >>= toList . flip M.lookup colrefs . Id
                                in
                                    (\fkId' sourceTableName' sourceColumnName' targetColRef' -> (fkId', (ColumnRef sourceTableName' sourceColumnName', targetColRef')))
                                        <$> fkId
                                        <*> sourceTableName
                                        <*> sourceColumnName
                                        <*> targetColRef)))

tables :: M.Map Id (ColumnRef, ColumnRef) -> Cursor -> [Table]
tables fks cursor = element "Project" cursor
    >>= child
    >>= element "Models"
    >>= child
    >>= element "DBTable"
    >>= (\c ->
        Table
        <$> (Id <$> attribute "Id" c)
        <*> attribute "Name" c
        <*> attribute "Documentation_plain" c
        <*> [columns c]
        <*> [relationships fks c])

relationships :: M.Map Id (ColumnRef, ColumnRef) -> Cursor -> [Relationship]
relationships fks cursor =
    concatMap (simpleRelationships fks cursor) [Outgoing, Incoming]

simpleRelationships :: M.Map Id (ColumnRef, ColumnRef) -> Cursor ->  RelationshipDirection -> [Relationship]
simpleRelationships fks cursor direction =
    let
        elName = case direction of
            Outgoing -> "ToSimpleRelationships"
            Incoming -> "FromSimpleRelationships"

        possiblySwapDirections = case direction of
            Outgoing -> id
            Incoming -> swap
    in
        child cursor
            >>= element elName
            >>= child
            >>= element "DBForeignKey"
            >>= fmap Id . attribute "Idref"
            >>= toList . flip M.lookup fks
            >>= return . possiblySwapDirections
            >>= (\(src, tgt) -> [Relationship direction src tgt])

columns :: Cursor -> [Column]
columns cursor = child cursor
        >>= element "ModelChildren"
        >>= child
        >>= element "DBColumn"
        >>= (\c ->
            Column
            <$> (Id <$> attribute "Id" c)
            <*> attribute "IdGenerator" c
            <*> attribute "Name" c
            <*> attribute "Documentation_plain" c
            <*> boolAttribute "Nullable" c
            <*> boolAttribute "PrimaryKey" c
            <*> attribute "Type" c
            <*> attribute "Length" c
            <*> boolAttribute "Unique" c)
